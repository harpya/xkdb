Specifications
========================





### Requirements

#### REQ.0001: The user should be able to create a *Bucket*

- If Bucket with same name already exists, then issue an exception **BucketAlreadyExistsException**.

#### REQ.0002: The user should be able to select an existent Bucket, to perform next operations

- If Bucket does not exists, then issue an exception **BucketNotExistentException**.


### Requirements




### Business Rules

#### BR.0001: Nothing can be created if there is no Bucket selected.

Applies to: general

#### BR.0002: The *name* of each item should be unique within selected *Bucket*.

Applies to: general

#### BR.0003: The *code* of each item is based on *name*.

Applies to: general

#### BR.0004:  A *Folder* can have only one parent.

Applies to: folder

#### BR.0005: A *Folder* can have multiple folders as children. 

Applies to: folder




