


[ ok ]  Create Bucket

[ ok ]  Create Folder
[ ok ]  Set parent Folder
[ ok ]  Get Parent Folder
[ ok ]  Get children Folders

[ ok ]  Create Label

[ ok ]  Assign Labels to a Folder
[ ok ]  Remove Labels from a Folder

[ ok ]  Get Folders assigned to Labels
[ ok ]  Get Labels assigned to a Folder


[ ok ]  Create Classifier
[ ok ]  Assign Label
[ ok ]  Remove Label
[  ]  Add Attributes specification to a Classifier
[  ]  Edit Attribute
[  ]  Remove Attribute
[  ]  Inherit a Classifier from another Classifier
[  ]  Get a Classifier blueprint (export JSON package with all metadata necessary to rebuild)
[  ]  



[  ]  Create Object
[  ]  Trigger an Event when a new Object is created (define filter) 
[  ]  Add Attributes (primitives) to an Object
[  ]  Trigger an Event when an Attribute is added/updated/removed from an Object (define filter) 
[  ]  Update Object data
[  ]  Search for Object name / ID
[  ]  Attach a Classifier to an Object
[  ]  Remove 


[  ]  Remove Bucker
[  ]  Remove Folder
