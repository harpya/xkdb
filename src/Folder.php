<?php

namespace harpya\xkdb;

/**
 * @author Eduardo Luz
 * @package Harpya \ XKDB
 */
class Folder
{
    use addons\HaveCode;
    use addons\HaveDescription;
    use addons\HaveAppReference;
    use addons\HaveLabels;

    // @rule BR.0004
    protected $parent;

    /**
     *
     */
    public function __construct($name)
    {
        $this->setName($name);
    }

    /**
     * @rule BR.0004
     */
    public function setParent($parent)
    {
        if (\is_string($parent)) {
            $target = $this->getApp()->getFolderByName($parent);

            if (!$target) {
                $target = $this->getApp()->getFolderByCode($parent);
            }
        } elseif (is_object($parent)) {
            $target = $this->getApp()->getFolderByCode($parent->getCode());
        }

        if ($target) {
            $this->parent = $target->getCode();
            $this->getApp()->registerParentFolder($target->getCode(), $this->getCode());
        } else {
            throw new \harpya\xkdb\exceptions\FolderException("Folder $parent not found.");
        }
    }

    /**
     * @rule BR.0004
     */
    public function getParent()
    {
        if ($this->parent) {
            return $this->getApp()->getFolderByCode($this->parent);
        }
        return null;
    }

    /**
     * @rule BR.0005
     */
    public function getChildren()
    {
        return $this->getApp()->getChildrenFolders($this->getCode());
    }
}
