<?php

namespace harpya\xkdb;

/**
 * @author Eduardo Luz
 * @package Harpya \ XKDB
 */
class Classifier
{
    use addons\HaveCode;
    use addons\HaveDescription;
    use addons\HaveAppReference;
    use addons\HaveLabels;

    /**
     *
     */
    public function __construct($name)
    {
        $this->setName($name);
    }
}
