<?php

namespace harpya\xkdb;

/**
 * @author Eduardo Luz
 * @package Harpya \ XKDB
 */
class Attribute
{
    use addons\HaveCode;
    use addons\HaveDescription;
    use addons\HaveAppReference;
    use addons\HaveLabels;

    const SPEC_TYPE = 'type';
    const SPEC_MIN_LENGTH = 'min_length';
    const SPEC_MAX_LENGTH = 'max_length';

    const TYPE_STRING = 'string';
    const TYPE_DATE = 'date';

    const SPEC_TYPE_FORMAT = 'type_format';

    protected $value;

    /**
     *
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     *
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     *
     */
    public function __construct($name, $specs = [], $objRef = null)
    {
        $this->setName($name);
        $this->parseSpecs($specs);
        $this->code = $this->getCode() . ':' . count($objRef->getApp()->getAll(Constants::KIND_ATTRIBUTE));
    }

    /**
     *  // TODO Implement this parser
     */
    protected function parseSpecs($specs)
    {
        // placeholder
    }
}
