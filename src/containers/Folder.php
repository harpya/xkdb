<?php

namespace harpya\xkdb\containers;

use harpya\xkdb\exceptions\ApplicationException;

trait Folder
{
    protected $folders;
    protected $parentFolders = [];

    /**
     *
     */
    protected function initFolders()
    {
        $this->folders = [];
    }

    /**
     *
     */
    public function addFolder($folder)
    {
        $this->checkSelectedBucket();

        $code = $folder->getCode();
        $this->folders[$code] = $folder->getName();
        $this->addItem($code, $folder);
    }

    /**
     *
     */
    public function getFoldersIndex()
    {
        return $this->folders;
    }

    /**
     *
     */
    public function getFolders()
    {
        return $this->getAll('folder');
    }

    /**
     *
     */
    public function getFolderByName($name)
    {
        $code = \harpya\xkdb\helpers\Code::resolveCode(\harpya\xkdb\Folder::class, $name);

        return $this->getFolderByCode($code);
    }

    /**
     *
     */
    public function getFolderByCode($code)
    {
        try {
            return $this->getItem($code);
        } catch (ApplicationException $ex) {
            throw new \harpya\xkdb\exceptions\FolderException("Folder $code not found.");
        }
    }

    /**
     * @rule BR.0004
     */
    public function registerParentFolder($parent, $child)
    {
        if (!$this->parentFolders[$parent]) {
            $this->parentFolders[$parent] = [];
        }
        $this->parentFolders[$parent][] = $child;
    }

    /**
     * @rule BR.0005
     */
    public function getChildrenFolders($parentCode)
    {
        return $this->parentFolders[$parentCode] ?? [];
    }
}
