<?php

namespace harpya\xkdb\containers;

use \harpya\xkdb\exceptions\ApplicationException;

trait Application
{
    protected $index = [];

    /**
     * Return the entire collection of Items
     */
    public function getAll($kind = false)
    {
        if ($kind) {
            if (isset($this->index[$kind])) {
                return $this->index[$kind];
            } else {
                return [];
            }
        }
        return $this->index;
    }

    /**
     *
     */
    public function addItem($code, $value)
    {
        $prefix = \harpya\xkdb\helpers\Code::getPrefixFromCode($code);
        if (!isset($this->index[$prefix])) {
            $this->index[$prefix] = [];
        }
        $this->index[$prefix][$code] = $value;
    }

    /**
     *
     */
    public function delItem($code)
    {
        $prefix = \harpya\xkdb\helpers\Code::getPrefixFromCode($code);

        if (isset($this->index[$prefix]) && isset($this->index[$prefix][$code])) {
            unset($this->index[$prefix][$code]);
        } else {
            throw new ApplicationException("Item with code $code not found");
        }
    }

    /**
     *
     */
    public function getItem($code)
    {
        $prefix = \harpya\xkdb\helpers\Code::getPrefixFromCode($code);

        if (isset($this->index[$prefix]) && isset($this->index[$prefix][$code])) {
            return $this->index[$prefix][$code];
        } else {
            throw new ApplicationException("Item with code $code not found");
        }
    }
}
