<?php

namespace harpya\xkdb\containers;

trait Bucket
{
    protected $buckets = [];
    protected $selectedBucket;

    /**
     * Set the selectedBucket with that one passed by argument.
     */
    public function setCurrentBucket($bucket)
    {
        $this->selectedBucket = $bucket;
        return $this;
    }

    /**
     * Returns the selected bucket.
     */
    public function getBucket()
    {
        return $this->selectedBucket ?? null;
    }

    /**
     * Check if there is no Bucket selected on this application. If so, issue an exception.
     * @rule REQ.0002
     */
    protected function checkSelectedBucket()
    {
        if (!$this->selectedBucket) {
            throw new \harpya\xkdb\exceptions\BucketException('Bucket not selected');
        }
    }

    /**
     *
     */
    public function getBucketByCode($code)
    {
        return $this->buckets[$code] ?? null;
    }

    /**
     *
     */
    public function registerBucket($bucket)
    {
        $this->buckets[$bucket->getCode()] = $bucket;
    }

    /**
     *
     */
    public function getAllBuckets()
    {
        return $this->buckets;
    }

    public function selectBucket($name)
    {
        $code = \harpya\xkdb\helpers\Code::resolveCode(harpya\xkdb\Bucket::class, $name);
        if (isset($this->buckets[$code])) {
            $this->selectBucket = $this->buckets[$code];
        } else {
            throw new \harpya\xkdb\exceptions\BucketException("Bucket $name does not exists");
        }
    }
}
