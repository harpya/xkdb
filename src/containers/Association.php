<?php

namespace harpya\xkdb\containers;

use \harpya\xkdb\KObject as ItemKObject;
use \harpya\xkdb\exceptions\ApplicationException;

trait Association
{
    public function attachAssociation($association)
    {
        $this->checkSelectedBucket();

        $code = $association->getCode();
        $this->addItem($code, $association);
    }
}
