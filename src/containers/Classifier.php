<?php

namespace harpya\xkdb\containers;

use \harpya\xkdb\exceptions\ApplicationException;

trait Classifier
{
    protected $classifiers = [];

    /**
     *
     */
    public function addClassifier($classifier)
    {
        $this->checkSelectedBucket();

        $code = $classifier->getCode();
        $this->classifiers[$code] = $classifier->getName();
        $this->addItem($code, $classifier);
        // $this->index[$classifier->getCode()] = $classifier;
    }

    /**
     *
     */
    public function getClassifiersCodes()
    {
        return $this->classifiers;
    }

    /**
     *
     */
    public function getClassifiers()
    {
        $list = [];
        foreach ($this->getClassifiersCodes() as $code => $name) {
            $list[$code] = $this->getItem($code);
        }
        return $list;
    }

    /**
     *
     */
    public function getClassifierByName($name)
    {
        $code = \harpya\xkdb\helpers\Code::resolveCode(\harpya\xkdb\Classifier::class, $name);

        return $this->getClassifierByCode($code);
    }

    /**
     *
     */
    public function getClassifierByCode($code)
    {
        try {
            return $this->getItem($code);
        } catch (ApplicationException $ex) {
            throw new \harpya\xkdb\exceptions\ClassifierException("classifier $code not found.");
        }
    }
}

//
