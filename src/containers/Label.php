<?php

namespace harpya\xkdb\containers;

use harpya\xkdb\exceptions\ApplicationException;

trait Label
{
    protected $listKeys = [];
    protected $labels = [];
    protected $labels2Items = [];

    /**
     *
     */
    public function addLabel($label)
    {
        $this->checkSelectedBucket();

        $key = $label->getKey();
        if (!isset($this->listKeys[$key])) {
            $this->listKeys[$key] = [];
        }
        $value = $label->getValue();
        if (!isset($this->listKeys[$key][$value])) {
            $this->listKeys[$key][$value] = 0;
        }

        $this->listKeys[$key][$value]++;

        $this->addItem($label->getCode(), $label);
    }

    /**
     *
     */
    public function getLabels()
    {
        return $this->listKeys;
    }

    /**
     *
     */
    public function getCodesAssignedToLabel($label, $kind = \harpya\xkdb\Constants::KIND_ALL)
    {
        if (\is_object($label)) {
            $name = $label->getName();
        }

        $list = $this->labels2Items[$name];

        if ($kind == \harpya\xkdb\Constants::KIND_ALL) {
            return $list;
        }

        $aux = $list ?? [];
        $list = array_filter($aux, function ($code) use ($kind) {
            if (\strpos($code, $kind) !== false) {
                return true;
            }
        });

        return $list;
    }

    /**
     *
     */
    public function assignLabelsToIndex($labelName, $objCode)
    {
        if (!isset($this->labels2Items[$labelName])) {
            $this->labels2Items[$labelName] = [];
        }
        $this->labels2Items[$labelName][$objCode] = $objCode;
        return $this;
    }

    /**
     *
     */
    public function removeLabelsToIndex($labelName, $objCode)
    {
        if (!isset($this->labels2Items[$labelName]) || empty($this->labels2Items[$labelName])) {
            return;
        }

        $this->labels2Items[$labelName] = array_filter($this->labels2Items[$labelName], function ($value) use ($objCode) {
            return ($value !== $objCode);
        });
    }

    /**
     *
     */
    public function getLabelByName($key, $value = null)
    {
        $name = \harpya\xkdb\Label::makeName($key, $value);

        $code = \harpya\xkdb\helpers\Code::resolveCode(\harpya\xkdb\Label::class, $name);

        // echo "\n\n code = $code ";
        return $this->getLabelByCode($code);
    }

    /**
     *
     */
    public function getLabelCodeByName($key, $value = null)
    {
        $name = \harpya\xkdb\Label::makeName($key, $value);

        // print_r($this->labels2Items[$name]);
        // exit;

        $code = \harpya\xkdb\helpers\Code::resolveCode(\harpya\xkdb\Label::class, $name);
        $this->getLabelByCode($code);
        return $code;
    }

    /**
     *
     */
    public function getLabelByCode($code)
    {
        try {
            return $this->getItem($code);
        } catch (ApplicationException $ex) {
            throw new \harpya\xkdb\exceptions\LabelException("label $code not found.");
        }
    }
}
