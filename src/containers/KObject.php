<?php

namespace harpya\xkdb\containers;

use \harpya\xkdb\KObject as ItemKObject;
use \harpya\xkdb\exceptions\KObjectException;
use harpya\xkdb\exceptions\ApplicationException;

trait KObject
{
    protected $objects = [];

    /**
     *
     */
    public function addObject($object)
    {
        $this->checkSelectedBucket();

        $code = $object->getCode();
        $this->objects[$code] = $object->getName();
        $this->addItem($code, $object);
    }

    /**
     *
     */
    public function removeObject($name)
    {
        $found = false;
        $this->checkSelectedBucket();

        $i = array_search($name, $this->objects);

        if ($i !== false) {
            $code = $i;
        } else { // 2nd chance to find the code...
            $code = \harpya\xkdb\helpers\Code::resolveCode(\harpya\xkdb\KObject::class, $name);
        }

        try {
            if (isset($this->objects[$code])) {
                unset($this->objects[$code]);
                $found = true;
            }

            $this->delItem($code);
            $found = true;
        } catch (ApplicationException $ex) {
            throw new KObjectException("Object name $name not found.");
        }

        if (!$found) {
            throw new KObjectException("Object name $name not found.");
        }
    }

    /**
     *
     */
    public function getObjectByName($name)
    {
        $code = \harpya\xkdb\helpers\Code::resolveCode(ItemKObject::class, $name);

        try {
            return $this->getObjectByCode($code);
        } catch (KObjectException $ex) {
            throw new KObjectException("Object name $name not found.");
        }
    }

    /**
     *
     */
    public function getObjectByCode($code)
    {
        try {
            return $this->getItem($code);
        } catch (ApplicationException $ex) {
            throw new KObjectException("Object $code not found.");
        }
    }
}
