<?php

namespace harpya\xkdb\containers;

use \harpya\xkdb\KObject as ItemKObject;
use \harpya\xkdb\exceptions\KObjectException;
use \harpya\xkdb\exceptions\AttributeException;
use \harpya\xkdb\exceptions\ApplicationException;

trait Attribute
{
    /**
     *
     */
    public function addAttribute($name, $specs = [])
    {
        $this->checkSelectedBucket();

        $attr = $this->getBuilder()->createAttribute($name, $specs);

        $code = $attr->getCode();
        $this->addItem($code, $attr);
    }

    /**
     *
     */
    public function attachAttribute($attribute)
    {
        $this->checkSelectedBucket();
        $code = $attribute->getCode();
        $this->addItem($code, $attribute);
        return $attribute;
    }

    /**
     *
     */
    public function removeAttribute($code)
    {
        try {
            $this->delItem($code);
        } catch (ApplicationException $ex) {
            throw new AttributeException("Attribute code $code not found");
        }
    }

    /**
     *
     */
    public function getAttribute($attribute)
    {
        if (\is_object($attribute)) {
            $name = $attribute->getName();
        } else {
            $name = $attribute;
        }

        $code = \harpya\xkdb\helpers\Code::resolveCode(\harpya\xkdb\Attribute::class, $name);
        $prefix = \harpya\xkdb\helpers\Code::getPrefixFromCode($code);

        $list = $this->getAll(\harpya\xkdb\Constants::KIND_ATTRIBUTE);

        $response = [];
        if (!empty($list)) {
            foreach ($list as $k => $v) {
                if (\strpos($k, $code) !== false) {
                    $response[$k] = $v;
                }
            }
        }

        return $response;
    }
}
