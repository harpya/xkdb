<?php


namespace harpya\xkdb\addons;

trait HaveAppReference
{
    protected $appID;

    public function setAppID($appID)
    {
        $this->appID = $appID;
    }

    public function getAppID()
    {
        return $this->appID;
    }

    public function getApp()
    {
        return \harpya\xkdb\App::getInstanceByID($this->getAppID());
    }
}
