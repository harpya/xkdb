<?php

namespace harpya\xkdb\addons;

/**
 *
 */
trait HaveCode
{
    protected $code;

    /**
     *
     */
    protected function setCode($code)
    {
        $this->code = \harpya\xkdb\helpers\Code::resolveCode(self::class, $code);
        return $this;
    }

    /**
     *
     */
    public function getCode()
    {
        return $this->code;
    }
}
