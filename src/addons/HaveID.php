<?php


namespace harpya\xkdb\addons;

trait HaveID
{
    protected $ID;

    public function setID($id)
    {
        $this->ID = $id;
        return $this;
    }

    public function getID()
    {
        return $this->ID;
    }
}
