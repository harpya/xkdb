<?php


namespace harpya\xkdb\addons;

trait HaveLabels
{
    // Store label's codes
    protected $assignedLabels = [];

    protected function getLabelName($label)
    {
        if (is_scalar($label)) {
            $name = $this->scalar2Name($label);
        } elseif (is_object($label)) {
            $name = $label->getName();
        }
        return $name;
    }

    /**
     *
     */
    public function assignLabel($label)
    {
        $name = $this->getLabelName($label);
        $this->assignedLabels[$name] = $name;

        $this->getApp()->assignLabelsToIndex($name, $this->getCode());
    }

    /**
     *
     */
    public function removeLabel($label)
    {
        $name = $this->getLabelName($label);
        $this->assignedLabels = array_filter($this->assignedLabels, function ($value) use ($name) {
            return ($value !== $name);
        });
        $this->getApp()->removeLabelsToIndex($name, $this->getCode());
    }

    /**
     *
     */
    public function getAssignedLabels()
    {
        return $this->assignedLabels;
    }

    /**
     * This $scalar may be the key:value or the string with the code.
     * This method will detect which type is, and will return the actual code
     */
    public function scalar2Name($scalar)
    {
        if (\strpos($scalar, '::') !== false) {
            return $this->index[$scalar] ?? null;
        }
        return $scalar;
    }
}
