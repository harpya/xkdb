<?php

namespace harpya\xkdb\addons;

/**
 *
 */
trait HaveAssociations
{
    //
    protected $associations = [];

    /**
     *
     */
    public function associateTo($value, $association = false)
    {
        $key = false;
        $targetName = false;
        if (\is_string($value)) {
            $key = $value;
        } elseif (is_object($value) && (in_array('harpya\xkdb\addons\HaveCode', class_uses($value)))) {
            $key = $value->getCode();
            $targetName = $value->getName();
        }

        if (!$targetName) {
            $obj = $this->getApp()->getItem($key);
            $targetName = $obj->getName();
        }

        $assoc = false;
        if ($association) {
            // TODO Validate this association
            $this->associations[$key] = $association;

            try {
                $assoc = $this->getApp()->getItem($association);
            } catch (\harpya\xkdb\exceptions\ApplicationException $ex) {

            }
            

        } else {
            $name = $this->getName() . ' -> ' . $targetName;
            $assoc = $this->getApp()->getBuilder()->createAssociation($name, $this->getCode(), $key);
            $this->associations[$key] = $assoc->getCode();
        }

            // TODO Validate the $assoc as Association object
            return $assoc;

    }


    /**
     *
     */
    public function getAssociations()
    {
        return $this->associations;
    }
}
