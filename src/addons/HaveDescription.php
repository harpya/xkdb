<?php

namespace harpya\xkdb\addons;

/**
 *
 */
trait HaveDescription
{
    protected $name;
    protected $description;

    /**
     *
     */
    protected function setName($name)
    {
        $this->name = $name;
        if (\method_exists($this, 'setCode')) {
            $this->setCode($name);
        }
        return $this;
    }

    /**
     *
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     *
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     *
     */
    public function getDescription()
    {
        return $this->description;
    }
}
