<?php

namespace harpya\xkdb;

/**
 * @author Eduardo Luz
 * @package Harpya \ XKDB
 */
class Bucket
{
    use addons\HaveCode;
    use addons\HaveDescription;
    use addons\HaveAppReference;

    public function __construct($name)
    {
        $this->setName($name);
    }
}
