<?php

namespace harpya\xkdb;

/**
 * @author Eduardo Luz
 * @package Harpya \ XKDB
 */
class KObject
{
    use addons\HaveCode;
    use addons\HaveDescription;
    use addons\HaveAppReference;
    use addons\HaveLabels;
    use addons\HaveAssociations;

    protected $lsRefAttributes = [];

    /**
     *
     */
    public function __construct($name)
    {
        $this->setName($name);
    }

    /**
     *
     */
    public function addAttribute($name, $specs = [])
    {
        $attr = $this->getApp()->getBuilder()->createAttribute($name, $specs);
        $code = $attr->getCode();
        // Store reference
        $this->lsRefAttributes[$name] = $code;
        return $attr;
    }

    /**
     *
     */
    public function attachAttribute($attribute)
    {
        $code = $attribute->getCode();
        $name = $attribute->getName();
        // Store reference
        $this->lsRefAttributes[$name] = $code;
        return $attribute;
    }

    /**
     *
     */
    public function removeAttribute($name)
    {
        $code = $this->lsRefAttributes[$name];
        $this->getApp()->removeAttribute($code);
        unset($this->lsRefAttributes[$name]);
    }

    /**
     *
     */
    public function getAttribute($name)
    {
        $code = $this->lsRefAttributes[$name] ?? null;
        if ($code) {
            $prefix = \harpya\xkdb\helpers\Code::getPrefixFromCode($code);
            $ret = $this->getApp()->getAll($prefix)[$code] ?? null;
        } else {
            throw new \harpya\xkdb\exceptions\AttributeException("Attribute $name not found");
        }

        return $ret;
    }

    /**
     *
     */
    public function getAttributes()
    {
        $list = [];

        $attrList = $this->getApp()->getAll(\harpya\xkdb\Constants::KIND_ATTRIBUTE);

        foreach ($this->lsRefAttributes as $name => $code) {
            $item = $attrList[$code];
            if ($item) {
                $list[] = $item;
            }
        }

        return $list;
    }
}
