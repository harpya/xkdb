<?php

namespace harpya\xkdb;

/**
 * @author Eduardo Luz
 * @package Harpya \ XKDB
 */
class Association extends KObject
{
    use addons\HaveCode;
    use addons\HaveDescription;
    use addons\HaveAppReference;
    use addons\HaveLabels;

    protected $originID;
    protected $targetID;

    /**
     *
     */
    public function setOrigin($objID)
    {
        $this->originID = $objID;
        return $this;
    }

    /**
     * 
     */
    public function getOrigin() {
        return $this->originID;
    }

    /**
     *
     */
    public function setTarget($objID)
    {
        $this->targetID = $objID;
        return $this;
    }

    /**
     * 
     */
    public function getTarget() {
        return $this->targetID;
    }

    /**
     *
     */
    public function __construct($name, $specs = [])
    {
        $this->setName($name);
        $this->parseSpecs($specs);
    }

    /**
     *
     */
    protected function parseSpecs($specs = [])
    {
        // Just a placeholder
    }
}
