<?php

namespace harpya\xkdb;

/**
 * @author Eduardo Luz
 * @package Harpya \ XKDB
 */
class Label
{
    use addons\HaveCode;
    use addons\HaveDescription;
    use addons\HaveAppReference;

    protected $key;
    protected $value;

    /**
     *
     */
    public function __construct($key, $value = null)
    {
        $this->setKey($key);
        $this->setValue($value);

        $name = self::makeName($key, $this->getValue()) ;
        $this->setName($name);
    }

    /**
     *
     */
    public static function makeName($key, $value)
    {
        static::normalizeValue($value);
        $name = "$key:$value";
        return $name;
    }

    /**
     *
     */
    protected function setKey($key)
    {
        $this->key = $key;
        return $this;
    }

    /**
     *
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     *
     */
    protected function setValue($value)
    {
        static::normalizeValue($value);
        $this->value = $value;
        return $this;
    }

    /**
     *
     */
    public static function normalizeValue(&$value = null)
    {
        if (\is_bool($value)) {
            $value = ($value) ? 'true' : 'false';
        }
    }

    /**
     *
     */
    public function getValue()
    {
        return $this->value;
    }
}
