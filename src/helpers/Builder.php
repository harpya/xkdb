<?php

namespace harpya\xkdb\helpers;

use \harpya\xkdb\Bucket;
use \harpya\xkdb\Folder;
use \harpya\xkdb\Label;

/**
 * @author Eduardo Luz
 * @package Harpya \ XKDB
 */
class Builder
{
    use \harpya\xkdb\addons\HaveAppReference;

    use \harpya\xkdb\helpers\builders\BucketBuilder;
    use \harpya\xkdb\helpers\builders\FolderBuilder;
    use \harpya\xkdb\helpers\builders\LabelBuilder;
    use \harpya\xkdb\helpers\builders\ClassifierBuilder;
    use \harpya\xkdb\helpers\builders\ObjectBuilder;
    use \harpya\xkdb\helpers\builders\AttributeBuilder;
    use \harpya\xkdb\helpers\builders\AssociationBuilder;
}
