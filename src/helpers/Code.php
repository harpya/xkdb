<?php

namespace harpya\xkdb\helpers;

use \harpya\xkdb\Constants;

class Code
{
    public static function getMapPrefix()
    {
        return [
            'harpya\xkdb\Folder' => Constants::KIND_FOLDER,
            'harpya\xkdb\Bucket' => Constants::KIND_BUCKET,
            'harpya\xkdb\Label' => Constants::KIND_LABEL,
            'harpya\xkdb\Classifier' => Constants::KIND_CLASSIFIER,
            'harpya\xkdb\KObject' => Constants::KIND_OBJECT,
            'harpya\xkdb\Attribute' => Constants::KIND_ATTRIBUTE,
            'harpya\xkdb\Association' => Constants::KIND_ASSOCIATION,
        ];
    }

    /**
     *
     */
    public static function getPrefixCode($className)
    {
        $map = static::getMapPrefix();

        if (isset($map[$className])) {
            return $map[$className];
        } else {
            return 'unknown';
        }
    }

    /**
     *
     */
    public static function resolveCode($className, $name, $objRef = null)
    {
        $prefix = static::getPrefixCode($className);
        return  static::makeCode($prefix, $name);
    }

    /**
     *
     */
    public static function makeCode($prefix, $name)
    {
        return $prefix . '::' . hash('sha256', $name);
    }

    /**
     *
     */
    public static function getPrefixFromCode($code)
    {
        list($prefix, $value) = explode('::', $code);
        return $prefix;
    }

    public static function getIDFromCode($code)
    {
        list($prefix, $value) = explode('::', $code);
        return $value;
    }

    /**
     * Validate a possible $code
     */
    public static function isValidCode($code)
    {
        // 1.) Min length is 67
        if (strlen($code) < 67) {
            return false;
        }

        // 2.) Check if have a prefix
        $prefix = static::getPrefixFromCode($code);

        if (!$prefix) {
            return false;
        }

        // 3.) check if is a valid prefix
        if (!\in_array($prefix, static::getMapPrefix())) {
            return false;
        }

        // 4.) ID part should have exactly 64 characters
        if (strlen(static::getIDFromCode($code)) === 64) {
            return true;
        } else {
            return false;
        }
    }
}
