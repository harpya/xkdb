<?php

namespace harpya\xkdb\helpers\builders;

use \harpya\xkdb\KObject;

trait ObjectBuilder
{
    /**
     * @return \harpya\xkdb\KObject
     */
    public function createObject($name, $value = null) : KObject
    {
        $object = new KObject($name, $value);
        $object->setAppID($this->getAppID());
        $this->getApp()->addObject($object);
        return $object;
    }
}
