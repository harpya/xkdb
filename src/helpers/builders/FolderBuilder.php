<?php

namespace harpya\xkdb\helpers\builders;

use \harpya\xkdb\Folder;

trait FolderBuilder
{
    /**
     * @return \harpya\xkdb\Folder
     */
    public function createFolder($name) : Folder
    {
        $folder = new Folder($name);
        $folder->setAppID($this->getAppID());
        $this->getApp()->addFolder($folder);
        return $folder;
    }
}
