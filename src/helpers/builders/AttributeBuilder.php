<?php

namespace harpya\xkdb\helpers\builders;

use \harpya\xkdb\Attribute;

trait AttributeBuilder
{
    /**
     * @return \harpya\xkdb\Attribute
     */
    public function createAttribute($name, $specs = null) : Attribute
    {
        $attr = new Attribute($name, $specs, $this);
        $attr->setAppID($this->getAppID());
        $this->getApp()->attachAttribute($attr);
        return $attr;
    }
}
