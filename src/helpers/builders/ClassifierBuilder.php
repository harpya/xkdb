<?php

namespace harpya\xkdb\helpers\builders;

use \harpya\xkdb\Classifier;

trait ClassifierBuilder
{
    /**
     * @return \harpya\xkdb\Classifier
     */
    public function createClassifier($name) : Classifier
    {
        $classifier = new Classifier($name);
        $classifier->setAppID($this->getAppID());
        $this->getApp()->addClassifier($classifier);
        return $classifier;
    }
}
