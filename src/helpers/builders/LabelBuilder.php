<?php

namespace harpya\xkdb\helpers\builders;

use \harpya\xkdb\Label;

trait LabelBuilder
{
    /**
     * @return \harpya\xkdb\Label
     */
    public function createLabel($name, $value = null) : Label
    {
        $label = new Label($name, $value);
        $label->setAppID($this->getAppID());
        $this->getApp()->addLabel($label);
        return $label;
    }
}
