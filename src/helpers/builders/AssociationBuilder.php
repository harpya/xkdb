<?php

namespace harpya\xkdb\helpers\builders;

use \harpya\xkdb\Association;

trait AssociationBuilder
{
    /**
     * @return \harpya\xkdb\Association
     *
     * @param $origin [OID | object's name | Object ]
     * @param $target [OID | object's name | Object ]
     * @param $specs array
     */
    public function createAssociation($name, $origin, $target, $specs = null) : Association
    {
        $originID = $this->getApp()->getCode($origin);
        $targetID = $this->getApp()->getCode($target);

        $assoc = $this->getAssociationInstance($name, $specs);
        $assoc->setAppID($this->getAppID());
        $assoc->setOrigin($originID);
        $assoc->setTarget($targetID);

        $this->getApp()->attachAssociation($assoc);

        $objOrigin = $this->getApp()->getItem($originID);
        $objTarget = $this->getApp()->getItem($targetID);

        $objOrigin->associateTo($targetID, $assoc->getCode());
        $objTarget->associateTo($originID, $assoc->getCode());

        return $assoc;
    }

    protected function getAssociationInstance($name, $specs)
    {
        return new Association($name, $specs, $this);
    }
}
