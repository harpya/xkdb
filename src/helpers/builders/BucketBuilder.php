<?php

namespace harpya\xkdb\helpers\builders;

use \harpya\xkdb\Bucket;

trait BucketBuilder
{
    /**
     *
     * @return \harpya\xkdb\Bucket
     */
    public function createBucket($name) : Bucket
    {
        $bucket = new Bucket($name);

        $this->checkBucketAlreadyExists($bucket);
        $this->getApp()->registerBucket($bucket);
        $bucket->setAppID($this->getAppID());

        $this->getApp()->setCurrentBucket($bucket);
        return $bucket;
    }

    /**
     *
     */
    protected function checkBucketAlreadyExists($bucket)
    {
        $response = $this->getApp()->getBucketByCode($bucket->getCode());
        if (!empty($response)) {
            throw new \harpya\xkdb\exceptions\BucketException("Already exists a bucket with name '$name'");
        }
    }
}
