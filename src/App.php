<?php

namespace harpya\xkdb;

class App
{
    use containers\Application;
    use addons\HaveID;
    use containers\Bucket;
    use containers\Folder;
    use containers\Label;
    use containers\Classifier;
    use containers\KObject;
    use containers\Attribute;
    use containers\Association;

    protected static $instances = [];
    protected $builder;

    /**
     * Returns the instance referenced by $id
     *
     * @return App
     */
    public static function getInstanceByID($id = '.') : App
    {
        if (!isset(static::$instances[$id])) {
            static::$instances[$id] = new App($id);
        }
        return static::$instances[$id];
    }

    /**
     *
     */
    public static function delInstanceByID($id = '.')
    {
        if (!isset(static::$instances[$id])) {
            unset(static::$instances[$id]);
        }
    }

    /**
     * Get the Builder instance for this app. Create one if does not exists.
     */
    public function getBuilder() : helpers\Builder
    {
        if (!$this->builder) {
            $this->builder = new helpers\Builder();
            $this->builder->setAppID($this->getID());
        }
        return $this->builder;
    }

    /**
     * Constructor method. Initialize the App ID
     */
    public function __construct($id)
    {
        $this->setID($id);
        $this->initFolders();
    }

    /**
     *
     */
    public function getCode($value, $type = false)
    {
        // 1st.: Get the origin ID
        if (\is_scalar($value)) {
            if (\harpya\xkdb\helpers\Code::isValidCode($value)) {
                return  $value;
            } else {
                $attempt2 = \harpya\xkdb\helpers\Code::resolveCode($type, $value);
                if ($attempt2) {
                    return $attempt2;
                }

                $attempt1 = \harpya\xkdb\helpers\Code::makeCode($type, $value);
                if ($attempt1) {
                    return $attempt1;
                }
            }
        } elseif (\is_object($value)) {
            // check if is a valid object
            if (in_array('harpya\xkdb\addons\HaveCode', class_uses($value))) {
                return $value->getCode();
            }
        }
    }
}
