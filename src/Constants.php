<?php

namespace harpya\xkdb;

class Constants
{
    const KIND_ALL = '*';
    const KIND_FOLDER = 'folder';
    const KIND_LABEL = 'label';
    const KIND_BUCKET = 'bucket';
    const KIND_CLASSIFIER = 'classifier';
    const KIND_OBJECT = 'object';
    const KIND_ATTRIBUTE = 'attribute';
    const KIND_ASSOCIATION = 'association';
}
