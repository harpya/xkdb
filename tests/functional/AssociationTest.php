<?php
require __DIR__ . '/../../vendor/autoload.php';

use PHPUnit\Framework\TestCase;
use harpya\xkdb\Attribute;

class AssociationTest extends TestCase
{
    public function testCreateSimpleAssociation()
    {
        $name = 'my first bucket';
        $app = \harpya\xkdb\App::getInstanceByID(__METHOD__);
        $builder = $app->getBuilder();
        $bucket = $app->getBuilder()->createBucket($name);

        $passportJohn = $builder->createObject('Passport-John');
        $passportAndrew = $builder->createObject('Passport-Andrew');
        $driverLicenseAndrew = $builder->createObject('Driver-License-Andrew');

        $personJohn = $builder->createObject('Person-John');
        $personAndrew = $builder->createObject('Person-Andrew');

        // Create an Association between $passportAndrew and $personAndrew
        $app->getBuilder()->createAssociation("Andrew's passport", $passportAndrew, $personAndrew);

        // Create an Association between $driverLicenseAndrew and $personAndrew
        $app->getBuilder()->createAssociation('Driver license belongs to Andrew', $personAndrew, $driverLicenseAndrew);

        // Create an Association between $passportJohn and $personJohn
        $passportJohn->associateTo($personJohn);

        $assocPassportJohn = $passportJohn->getAssociations();
        $this->assertCount(1, $assocPassportJohn);

        $assocPersonAndrew = $personAndrew->getAssociations();
        $this->assertCount(2, $assocPersonAndrew);
    }


    public function testCreateAssociationWithAttributes() {
        $name = 'my project';
        $app = \harpya\xkdb\App::getInstanceByID(__METHOD__);
        $builder = $app->getBuilder();
        $bucket = $app->getBuilder()->createBucket($name);

        $digitalOcean = $builder->createObject('DigitalOcean');
        $serverAlpha = $builder->createObject('server-Alpha');
        $serverBeta = $builder->createObject('server-Beta');

        $assoc = $serverAlpha->associateTo($digitalOcean);
        $assoc->addAttribute('Created at', [
            Attribute::SPEC_TYPE => Attribute::TYPE_DATE,
                Attribute::SPEC_TYPE_FORMAT => 'Y-M-D',
            ]);;

        $this->assertEquals('server-Alpha -> DigitalOcean', $assoc->getName());

        $this->assertCount(1, $assoc->getAttributes());

        $this->assertEquals($digitalOcean->getCode(), $assoc->getTarget());
        $this->assertEquals($serverAlpha->getCode(), $assoc->getOrigin());


        $digitalOcean->associateTo($serverBeta);

        $this->assertCount(2, $digitalOcean->getAssociations());
    }
}
