<?php

use PHPUnit\Framework\TestCase;

class LabelTest extends TestCase
{
    public function testFailCreateLabelWithoutBucket()
    {
        $builder = \harpya\xkdb\App::getInstanceByID('+')->getBuilder();

        $this->expectException(\harpya\xkdb\exceptions\BucketException::class);
        $builder->createLabel('billable', true);
    }


    public function testCreateLabel()
    {
        $builder = \harpya\xkdb\App::getInstanceByID('x')->getBuilder();
        $bucket = \harpya\xkdb\App::getInstanceByID('x')->getBuilder()->createBucket('test');

        $label1 = $builder->createLabel('billable', true);
        $label2 = $builder->createLabel('instance', 'dev');
        $label3 = $builder->createLabel('instance', 'qa');
        $label4 = $builder->createLabel('instance', 'prod');

        $labels = \harpya\xkdb\App::getInstanceByID('x')->getLabels();

        $this->assertArrayHasKey('instance', $labels);
        $this->assertArrayHasKey('billable', $labels);
    }
}
