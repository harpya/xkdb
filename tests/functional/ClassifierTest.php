<?php

use PHPUnit\Framework\TestCase;

class ClassifierTest extends TestCase
{
    public function testCreateClassifier()
    {
        $app = \harpya\xkdb\App::getInstanceByID(__METHOD__);
        $builder = $app->getBuilder();

        $bucket = $builder->createBucket('test');

        $builder->createClassifier('Book');

        $classifiersCodes = $app->getClassifiersCodes();

        $this->assertCount(1, $classifiersCodes);
    }


    /**
     *
     * Steps:
     * 1. Preparation
     * 1.1. Create the Bucket 'test'
     * 1.2. Create the classifiers:
     * 1.2.1. 'Driver License'
     * 1.2.2. 'Book'
     * 1.2.3. 'Resume'
     * 1.2.4. 'Birth Certificate'
     * 1.3. Create label 'ID'
     * 1.4. Apply the label 'ID' to Classifier 'Driver License'
     * 1.5. Create a label 'Document' and assign to 'Resume'
     * 2. Tests
     * 2.1. Check if have 2 Classifiers assigned to label ID
     * 2.2. Check if have 2 Classifiers assigned to label Document
     *
     */
    public function testAssignLabelToClassifiers()
    {
        // 1. Setup
        $app = \harpya\xkdb\App::getInstanceByID(__METHOD__);
        $builder = $app->getBuilder();

        $bucket = $builder->createBucket('test');

        $classDL = $builder->createClassifier('Driver License');
        $builder->createClassifier('Book');
        $builder->createClassifier('Resume');
        $classBC = $builder->createClassifier('Birth Certificate');

        $labelID = $builder->createLabel('ID', true);


        $classifiersCodes = $app->getClassifiersCodes();

        $classDL->assignLabel(
            $labelID
        );

        $id = $app->getLabelByName('ID', true);

        $classBC->assignLabel(
            $id
        );


        $app->getClassifierByName('Resume')->assignLabel(
            $builder->createLabel('Document')
        );

        $lblDocument = $app->getLabelByName('Document');


        $app->getClassifierByName('Book')->assignLabel($lblDocument);


        // 2. Tests
        $assignedItemsL1 = $app->getCodesAssignedToLabel($labelID, \harpya\xkdb\Constants::KIND_CLASSIFIER);
        $this->assertCount(2, $assignedItemsL1);

        
        $assignedItemsL2 = $app->getCodesAssignedToLabel($lblDocument, \harpya\xkdb\Constants::KIND_CLASSIFIER);

        $this->assertCount(2, $assignedItemsL2);
    }


    /**
     *
     *
     * 1. Setup
     * 1.1. Create Bucket
     * 1.2. Create Classifiers 'ID', 'Book' and 'Document'
     * 1.3. Create Labels 'Personal' and 'Professional'
     * 1.4. Assign Label 'Personal' to all 3 Classifiers
     * 1.5. Assign Label 'Professional' to Classifier 'Document'
     * 2. Test 1
     * 2.1. Verify that Label 'Personal' have 3 Classifiers assigned;
     * 2.2. Verify that label 'Professional' have 1 Classifier assigned;
     * 3. Make adjustments
     * 3.1. Remove Label 'Personal' from Classifier 'Document'
     * 3.2. Remove Label 'Professional' from Classifier 'Document'
     * 4. Test 2
     * 4.1. Verify that there is zero Classifiers assigned with Label 'Professional'
     * 4.2. Verify that there is 2 Classifiers assigned with Label 'Personal'
     * 4.3. Verify that Classifier 'ID' have 1 Label assigned
     * 4.4. Verify if Classifier 'Document' have 0 labels assigned to it.
     *
     */
    public function testAddRemoveLabelsToClassifiers()
    {
        // 1. Setup
        $app = \harpya\xkdb\App::getInstanceByID(__METHOD__);
        $builder = $app->getBuilder();

        $bucket = $builder->createBucket('test');

        $clsID = $builder->createClassifier('ID');
        $clsBook = $builder->createClassifier('Book');
        $clsDocument = $builder->createClassifier('Document');

        $clsID->assignLabel(
            $builder->createLabel('Personal')
        );

        $clsBook->assignLabel($app->getLabelByName('Personal'));
        $clsDocument->assignLabel($app->getLabelByName('Personal'));

        $clsDocument->assignLabel($app->getBuilder()->createLabel('Professional'));

        // 2. Tests 1
        $assignedItemsL1 = $app->getCodesAssignedToLabel($app->getLabelByName('Personal'), \harpya\xkdb\Constants::KIND_CLASSIFIER);
        $this->assertCount(3, $assignedItemsL1);

        $assignedItemsL2 = $app->getCodesAssignedToLabel($app->getLabelByName('Professional'), \harpya\xkdb\Constants::KIND_CLASSIFIER);
        $this->assertCount(1, $assignedItemsL2);

        // 3. Make Adjustments
        $clsDocument->removeLabel($app->getLabelByName('Personal'));
        $clsDocument->removeLabel($app->getLabelByName('Professional'));

        // 4. Tests 2
        $assignedItemsL3 = $app->getCodesAssignedToLabel($app->getLabelByName('Professional'), \harpya\xkdb\Constants::KIND_CLASSIFIER);
        $this->assertCount(0, $assignedItemsL3);

        $assignedItemsL4 = $app->getCodesAssignedToLabel($app->getLabelByName('Personal'), \harpya\xkdb\Constants::KIND_CLASSIFIER);
        $this->assertCount(2, $assignedItemsL4);

        $labels = $clsID->getAssignedLabels();
        $this->assertCount(1, $labels);

        $labels = $clsDocument->getAssignedLabels();
        $this->assertCount(0, $labels);
    }

    /**
     * // TODO Implement this test
     *
     * 1. Setup
     * 1.1. Create Bucket.
     * 1.2. Create Folders 'Personal' and 'Professional'.
     * 1.3. Create a Folder 'Project X' in Folder 'Professional'.
     * 1.4. Create Folders 'Travels' and 'Documents' in Folder 'Personal'.
     * 1.5. Create some Classifiers, and assign to each Folder
     * 1.5.1. Create Classifiers 'Vacation Trip', 'Camping' and 'International travel', and assign to 'Personal'->'Travels'
     * 1.5.2. Create Classifiers 'ID' and 'Passport' and put inside of Folder 'Personal'->'Documents'
     * 1.5.3. Create Classifier 'Meeting Log' and put in 'Professional' Folder.
     *
     * 2. Tests
     * 2.1. Verify that the number of Classifiers within Folder 'Personal'->'Travels' is 3.
     * 2.2. Verify that the number of Classifiers within Folder 'Personal'->'Documents' is 2.
     * 2.3. Verify that the number of Classifiers within Folder 'Personal' is 0, if not include sub-folders, and 5 if include.
     * 2.4. Verify that the number of Classifiers within Folder 'Professional' is 1.
     * 2.5. Verify that the number of Classifiers within Folder 'Professional'->'Project X' is 0.
     * 2.6. Verify that the number of Classifiers in Root Folder is 0, if not include sub-folders, and 6 if include.
     * 2.7. Get the current Folder where Classifier 'Vacation Trip' is located.
     * 2.7.1 Check if is 'Personal'->'Travels'
     *
     */
    public function testOrganizeClassifiersInFolders()
    {
        $this->assertTrue(true); // placeholder
    }
}
