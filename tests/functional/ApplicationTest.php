<?php
require(__DIR__ ."/../../vendor/autoload.php");

use PHPUnit\Framework\TestCase;

class ApplicationTest extends TestCase
{
    public function testCreateBucket()
    {
        $name = 'my first bucket';

        $bucket = \harpya\xkdb\App::getInstanceByID()->getBuilder()->createBucket($name);

        $this->assertEquals('harpya\\xkdb\\Bucket', get_class($bucket));
        $this->assertEquals($name, $bucket->getName());
    }



    public function testAssignLabelToFolder()
    {
        $builder = \harpya\xkdb\App::getInstanceByID(__METHOD__)->getBuilder();
        $bucket = \harpya\xkdb\App::getInstanceByID(__METHOD__)->getBuilder()->createBucket('test');

        $label1 = $builder->createLabel('billable', true);
        $label2 = $builder->createLabel('billable', false);

        $folder1 = $builder->createFolder('folder 1');
        $folder2 = $builder->createFolder('folder 2');

        $folder1->assignLabel($label1);
        $folder2->assignLabel($label1);
        $folder1->assignLabel($label2);

        $assignedItems = \harpya\xkdb\App::getInstanceByID(__METHOD__)->getCodesAssignedToLabel($label1, \harpya\xkdb\Constants::KIND_FOLDER);
        $this->assertCount(2, $assignedItems);
    }

    public function testRemoveLabelToFolder()
    {
        $app = \harpya\xkdb\App::getInstanceByID(__METHOD__);
        $builder = $app->getBuilder();
        $bucket = $builder->createBucket('test');

        $label1 = $builder->createLabel('billable', true);
        $label2 = $builder->createLabel('billable', false);
        $label3 = $builder->createLabel('context1');

        $label4 = $builder->createLabel('engineering');

        $folder1 = $builder->createFolder('folder 1');
        $folder2 = $builder->createFolder('folder 2');
        $folder3 = $builder->createFolder('folder 3');

        $folder1->assignLabel($label1);
        $folder1->assignLabel($label2);
        $folder1->assignLabel($label3);
        $folder1->assignLabel($label3); // duplicated - should be ignored

        $folder2->assignLabel($label3);
        $folder2->assignLabel($label4);

        $folder3->assignLabel($label4);

        // Remove label3 assignment from folder2
        $folder2->removeLabel($label3);


        // Check if all 3 labels are assigned to folder1
        $assignedItemsF1 = $folder1->getAssignedLabels();
        $this->assertCount(3, $assignedItemsF1);


        // Check if all 2 labels are assigned to folder2
        $assignedItemsF2 = $folder2->getAssignedLabels();
        $this->assertCount(1, $assignedItemsF2);


        // Check if only folder1 is assigned to label1
        $assignedItemsL1 = $app->getCodesAssignedToLabel($label1, \harpya\xkdb\Constants::KIND_FOLDER);
        $this->assertCount(1, $assignedItemsL1);

        // Check if only folder1 is assigned to label2
        $assignedItemsL2 = $app->getCodesAssignedToLabel($label2, \harpya\xkdb\Constants::KIND_FOLDER);
        $this->assertCount(1, $assignedItemsL2);


        // Check if only folder1 is assigned to label3
        $assignedItemsL3 = $app->getCodesAssignedToLabel($label3, \harpya\xkdb\Constants::KIND_FOLDER);
        $this->assertCount(1, $assignedItemsL3);

        // Check if both folder2 and folder3 are assigned to label4
        $assignedItemsL4 = \harpya\xkdb\App::getInstanceByID(__METHOD__)->getCodesAssignedToLabel($label4, \harpya\xkdb\Constants::KIND_FOLDER);
        $this->assertCount(2, $assignedItemsL4);
    }
}
