<?php
require(__DIR__ ."/../../vendor/autoload.php");

use PHPUnit\Framework\TestCase;

use harpya\xkdb\exceptions\BucketException;
use harpya\xkdb\exceptions\ApplicationException;

class FolderTest extends TestCase
{
    public function testCreateFolderWithoutBucketSelected()
    {
        $builder = \harpya\xkdb\App::getInstanceByID(__METHOD__)->getBuilder();
        $this->expectException(BucketException::class);
        $builder->createFolder('folder 1');
    }

    /**
     * Create and returns a Folder instance.
     */
    public function testCreateFolder()
    {
        $app = \harpya\xkdb\App::getInstanceByID(__METHOD__);
        $builder = $app->getBuilder();

        $bucket = $builder->createBucket('test');

        $builder->createFolder('folder 1');
        $builder->createFolder('folder 2');

        $folders = $app->getFolders();

        $this->assertCount(2, $folders);

        $folder1 = $app->getFolderByName('folder 1');
        $this->assertEquals(\harpya\xkdb\Folder::class, get_class($folder1));
    }

    public function testSetParentFolder()
    {
        $builder = \harpya\xkdb\App::getInstanceByID(__METHOD__)->getBuilder();

        $bucket = $builder->createBucket('test');

        $folder1 = $builder->createFolder('folder 1');
        $folder2 = $builder->createFolder('folder 2');
        $folder3 = $builder->createFolder('folder 3');
        $folder4 = $builder->createFolder('folder 4');

        $folder1->setParent($folder2->getName());
        $folder4->setParent($folder2->getName());
        $folder2->setParent($folder3);

        $parentFolder1 = $folder1->getParent();
        $parentFolder2 = $folder2->getParent();
        $parentFolder3 = $folder3->getParent();

        $this->assertEquals(\harpya\xkdb\Folder::class, get_class($parentFolder1));
        $this->assertEquals(\harpya\xkdb\Folder::class, get_class($parentFolder2));
        $this->assertNull($parentFolder3);

        $childrenFolder2 = $folder2->getChildren();
        $this->assertCount(2, $childrenFolder2);
        $this->assertCount(1, $folder3->getChildren());
    }

    public function testGetInexistentFolder()
    {
        $builder = \harpya\xkdb\App::getInstanceByID()->getBuilder();

        $this->expectException(\harpya\xkdb\exceptions\FolderException::class);
        \harpya\xkdb\App::getInstanceByID()->getFolderByName('does not exists');
    }
}
