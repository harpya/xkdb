<?php

use PHPUnit\Framework\TestCase;

use harpya\xkdb\exceptions\BucketException;

class BucketTest extends TestCase
{
 
    /**
     * @rule REQ.0001
     */
    public function testBucketAlreadyExistsException()
    {
        $name = 'my first bucket';

        $bucket = \harpya\xkdb\App::getInstanceByID(__METHOD__)->getBuilder()->createBucket($name);

        $this->expectException(BucketException::class);
        $bucket2 = \harpya\xkdb\App::getInstanceByID(__METHOD__)->getBuilder()->createBucket($name);
    }


    /**
     *
     */
    public function testSelectBucket()
    {
        $name = 'my first bucket';

        $bucket = \harpya\xkdb\App::getInstanceByID(__METHOD__)->getBuilder()->createBucket($name);

        $resp = \harpya\xkdb\App::getInstanceByID(__METHOD__)->getBucket();

        $this->assertTrue(is_object($resp));
        $this->assertEquals($name, $resp->getName());
    }

    /**
     *
     */
    public function testSelectedInvalidBucket()
    {
        $this->expectException(BucketException::class);
        \harpya\xkdb\App::getInstanceByID(__METHOD__)->selectBucket('anything');
    }
}
