<?php

use PHPUnit\Framework\TestCase;
use harpya\xkdb\exceptions\KObjectException;
use harpya\xkdb\exceptions\AttributeException;
use harpya\xkdb\Attribute;

class ObjectsTest extends TestCase
{

    /**
     *
     * 1. Setup
     * 1.1. Create App / Bucket
     * 1.2. Create a simple Object named 'MyPassport'
     * 1.3. Create a simple Object named 'MyWifePassport'
     * 2. Test
     * 2.1. Verify that exists 2 Objects on current Bucket
     * 2.2. Verify that 'MyPassport' is present
     *
     */
    public function testCreateObject()
    {
        // Step 1
        $app = \harpya\xkdb\App::getInstanceByID(__METHOD__);
        $builder = $app->getBuilder();

        $bucket = $builder->createBucket('test');

        $builder->createObject('MyPassport');
        $builder->createObject('MyWifePassport');

        $list = $app->getAll(\harpya\xkdb\Constants::KIND_OBJECT);

        // Step 2
        $this->assertCount(2, $list);
    }


    /**
     * 1. Setup
     * 1.1. Create 10 Objects from an Array
     * 1.2. Remove 3 of those objects
     * 2. Tests
     * 2.1. Check if have 7 Objects on Bucket
     * 2.2. Try to remove a non-existent Object (expected Exception)
     */
    public function testCreateAndRemoveMultipleObjects()
    {
        // Step 1
        $app = \harpya\xkdb\App::getInstanceByID(__METHOD__);
        $builder = $app->getBuilder();
        
        $bucket = $builder->createBucket('test');

        $myArray = ['A','B','C','D','E','F','G','H','I','J'];

        foreach ($myArray as $item) {
            $builder->createObject($item);
        }

        $app->removeObject('D');
        $app->removeObject('F');
        $app->removeObject('H');

        // Step 2
        $list = $app->getAll(\harpya\xkdb\Constants::KIND_OBJECT);

        $this->assertCount(7, $list);

        $objG = $app->getObjectByName('G');
        $this->assertEquals('G', $objG->getName());

        // If Object does not exists
        $this->expectException(KObjectException::class);
        $app->removeObject('H');
    }


    /**
     *
     * 1. Preparation
     * 1.1. Create App/Bucket
     * 1.2. Create Object 'Passport'
     * 1.3. Add Attributes: 'First Name', 'Last Name', 'Document Number', 'Issued at', 'Valid until', 'Birth date', 'Class Document'
     * 1.4. Update attribute 'Valid until'
     * 1.5. Remove attribute 'Class Document'
     * 2. Test
     * 2.1. Get list of attributes - should count 6 items
     * 2.2. Get 'Class Document'  - should fail
     * 2.3. Get value of 'Issued at' - should match
     *
     */
    public function testCreateObjectAttributes()
    {
        // Step 1
        $app = \harpya\xkdb\App::getInstanceByID(__METHOD__);
        $builder = $app->getBuilder();
                
        $bucket = $builder->createBucket('test');
        $passport = $builder->createObject('Passport');

        // An Attribute may be duplicated on same Bucket.
        $documentNumber = $builder->createAttribute('Document Number', [
                    Attribute::SPEC_TYPE => Attribute::TYPE_STRING,
                    Attribute::SPEC_MIN_LENGTH => 5,
                    Attribute::SPEC_MAX_LENGTH => 10,
                ]);

        $documentNumber->setValue('AB123456');

        $passport->attachAttribute($documentNumber);
        $firstName = $passport->addAttribute('First Name', [
            Attribute::SPEC_TYPE => Attribute::TYPE_STRING
        ]);
        $firstName->setValue('John');

        $passport->addAttribute('Last Name', [
            Attribute::SPEC_TYPE => Attribute::TYPE_STRING
        ])->setValue('Doe');

        $passport->addAttribute('Issued at', [
            Attribute::SPEC_TYPE => Attribute::TYPE_DATE,
            Attribute::SPEC_TYPE_FORMAT => 'Y-M-D',
        ])->setValue('2018-06-15');
        
        $passport->addAttribute('Valid until', [
            Attribute::SPEC_TYPE => Attribute::TYPE_DATE,
            Attribute::SPEC_TYPE_FORMAT => 'Y-M-D',
        ])->setValue('2023-06-14');

        $passport->addAttribute('Birth date', [
            Attribute::SPEC_TYPE => Attribute::TYPE_DATE,
            Attribute::SPEC_TYPE_FORMAT => 'Y-M-D',
        ])->setValue('1975-02-01');

        $passport->addAttribute('Class Document', [
            Attribute::SPEC_TYPE => Attribute::TYPE_STRING,
        ])->setValue('A');

        $passport->getAttribute('Valid until')->setValue('2023-06-15');
  
        $passport->removeAttribute('Class Document');
        
        // Step 2
        $list = $passport->getAttributes();
        $this->assertCount(6, $list);

        $validUntil = $passport->getAttribute('Valid until')->getValue();
        $this->assertEquals('2023-06-15', $validUntil);

        $this->expectException(AttributeException::class);
        $passport->getAttribute('Class Document');
    }



    /**
     * 1. Preparation
     * 1.1. Create App / Bucket
     * 1.2. Create Object 'Passport' with attributes 'Number', 'Issued at' and 'Valid until'
     * 1.3. Create Object 'Driver License', with attribute 'Number' and 'Valid until'
     * 1.4. Create Label 'expires:true'
     * 1.5. Assign label 'expires:true' to both attributes 'Valid until'
     * 1.6. Create Label 'expired:true' and assign it to Passport.
     * 1.7. Get all items with Label 'expires:true' assigned ($expires)
     * 1.8. Get all items with Label 'expired:true' assigned ($expired)
     *
     * 2. Test
     * 2.1. count($expires) = 2
     * 2.2. count($expired) = 1
     * 2.3. Item referenced by $expired is KObject, with name 'Passport'
     * 2.4. Items referenced by $expires are Attributes.
     */
    public function testAssignLabelsToObjectsAndAttributes()
    {
        // Step 1
        $app = \harpya\xkdb\App::getInstanceByID(__METHOD__);
        $builder = $app->getBuilder();
                        
        $bucket = $builder->createBucket('test');
        $passport = $builder->createObject('Passport');
        
        $documentNumber = $builder->createAttribute('Number', [
                            Attribute::SPEC_TYPE => Attribute::TYPE_STRING,
                        ]);
        
        $passport->attachAttribute($documentNumber);

        $passportIssuedAt = $builder->createAttribute('Issued at', [
                Attribute::SPEC_TYPE => Attribute::TYPE_DATE,
                    Attribute::SPEC_TYPE_FORMAT => 'Y-M-D',
            ]);

        $passport->attachAttribute($passportIssuedAt)->setValue('2018-06-15');

        $passportValidUntil = $builder->createAttribute('Valid until', [
                Attribute::SPEC_TYPE => Attribute::TYPE_DATE,
                    Attribute::SPEC_TYPE_FORMAT => 'Y-M-D',
            ]);

        $passport->attachAttribute($passportValidUntil)->setValue('2018-06-14');
                
        //
        $driverLicense = $builder->createObject('Driver License');


        $documentNumber = $builder->createAttribute('Number', [
            Attribute::SPEC_TYPE => Attribute::TYPE_STRING,
        ]);

        $dlIssuedAt = $builder->createAttribute('Issued at', [
        Attribute::SPEC_TYPE => Attribute::TYPE_DATE,
            Attribute::SPEC_TYPE_FORMAT => 'Y-M-D',
        ]);



        $driverLicense->attachAttribute($dlIssuedAt)->setValue('2018-06-15');

        $dlValidUntil = $builder->createAttribute('Valid until', [
        Attribute::SPEC_TYPE => Attribute::TYPE_DATE,
            Attribute::SPEC_TYPE_FORMAT => 'Y-M-D',
        ]);

        $dtPassportIsValidUntil = '2018-01-05';

        $passport->attachAttribute($dlValidUntil)->setValue($dtPassportIsValidUntil);

        $labelExpires = $builder->createLabel('expires', true);
        $labelExpired = $builder->createLabel('expired', true);

        $passportValidUntil->assignLabel($labelExpires);
        $dlValidUntil->assignLabel($labelExpires);
        $passport->assignLabel($labelExpired);

        $assignedItemsExpires = $app->getCodesAssignedToLabel($labelExpires, \harpya\xkdb\Constants::KIND_ATTRIBUTE);
        $assignedItemsExpired = $app->getCodesAssignedToLabel($labelExpired, \harpya\xkdb\Constants::KIND_OBJECT);

        // $driverLicense
        $this->assertCount(2, $assignedItemsExpires);
        $this->assertCount(1, $assignedItemsExpired);

        $lsValidUntil = $passport->getAttribute('Valid until');
        $this->assertTrue(!empty($lsValidUntil));
        $this->assertTrue(is_object($lsValidUntil));
        $this->assertEquals(\harpya\xkdb\Attribute::class, get_class($lsValidUntil));
        $this->assertEquals($dtPassportIsValidUntil, $lsValidUntil->getValue());

        $lsAllValidUntil = $app->getAttribute('Valid until');
        $this->assertTrue(!empty($lsAllValidUntil));
        $this->assertTrue(is_array($lsAllValidUntil));
        $this->assertCount(2, $lsAllValidUntil);
    }
}
