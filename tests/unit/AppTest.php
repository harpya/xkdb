<?php

use PHPUnit\Framework\TestCase;

class AppTest extends TestCase
{
    public function testGetCodeScalar()
    {
        $objName = 'AbcDef';

        $app = \harpya\xkdb\App::getInstanceByID(__METHOD__);

        $app->getBuilder()->createBucket('test');

        $obj = $app->getBuilder()->createObject($objName);

        $prefix = \harpya\xkdb\helpers\Code::getPrefixCode(get_class($obj));
        $fullCode = \harpya\xkdb\helpers\Code::resolveCode(get_class($obj), $objName);

        $code = $app->getCode($fullCode, $prefix);

        $this->assertEquals($obj->getCode(), $code);

        $code = $app->getCode($objName, get_class($obj));

        $this->assertEquals($obj->getCode(), $code);

        $code = $app->getCode($objName, 'invalid');

        $this->assertTrue(strpos($code, 'unknown') !== false);
    }

    public function testGetCodeObject()
    {
        $objName = 'AbcDef';

        $app = \harpya\xkdb\App::getInstanceByID(__METHOD__);

        $app->getBuilder()->createBucket('test');

        $obj = $app->getBuilder()->createObject($objName);

        $code = $app->getCode($obj);

        $this->assertEquals($obj->getCode(), $code);
    }
}
