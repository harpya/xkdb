<?php

use PHPUnit\Framework\TestCase;

class BuilderTest extends TestCase
{
    public function getApp($appID='-')
    {
        $app = \harpya\xkdb\App::getInstanceByID($appID);
        return $app;
    }

    public function getBuilder()
    {
        return $this->getApp()->getBuilder();
    }

    public function testCreateBucket()
    {
        $builder = $this->getBuilder();

        $name = 'my first bucket';

        $bucket = $builder->createBucket($name);

        $this->assertEquals('harpya\\xkdb\\Bucket', get_class($bucket));
        $this->assertEquals($name, $bucket->getName());
    }

    public function testSetBucketDescription()
    {
        $builder = $this->getBuilder();

        $description = 'My Description';

        $bucket = $builder->createBucket('test '.__METHOD__);
        $bucket->setDescription($description);

        $this->assertEquals($description, $bucket->getDescription());
    }

    public function testBucketWasAddedToApplication()
    {
        $bucket = $this->getBuilder()->createBucket('test '.__METHOD__);
        $this->assertEquals($this->getApp()->getID(), $bucket->getAppID());
    }
}
